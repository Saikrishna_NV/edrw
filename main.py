import edrw
import createHGraph
import numpy as np
import random

Ytrue = np.genfromtxt("sampleLabels.csv",delimiter=',')
N = len(Ytrue)
Y = []

for i in range(N):
	if(random.random() < 0.9):
		Y.append(-1)
	else:
		Y.append(Ytrue[i])

X = np.genfromtxt("sampleData.csv",delimiter=',')
H = createHGraph.catToHypergraph(X)
H = createHGraph.processHypergraph(H)

W = createHGraph.assignWeights(H, Y)
E = len(W)

edrw.edrw(H, W, N, E, Y, Ytrue)