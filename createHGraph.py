import numpy as np
from scipy.sparse import coo_matrix as co
from sklearn.cluster import AgglomerativeClustering as acg

def catToHypergraph(X):
	p = len(X[0])
	N = len(X)
	edgeNum = 0
	H = [[] for i in range(N)]
	for i in range(p):
		attrValues = np.unique(X[:, i])
		for val in attrValues:
			for j in range(N):
				if(X[j][i] == val):
					H[j].append(1)
				else:
					H[j].append(0)
			edgeNum += 1
	return H

def processHypergraph(H):
	e = len(H[0])
	N = len(H)
	i = 0
	while(i < e):
		# check if there are edges with only one node or all nodes
		count = 0
		for j in range(N):
			if(H[j][i] == 1):
				count += 1
		if((count == 0) or (count == N)):
			# remove i-th column
			for x in H:
				del x[i]
			e = e - 1
		else:
			i += 1

	return H

def assignWeights(H, Y):
	e = len(H[0])
	N = len(H)
	W = [0 for i in range(e)]

	labelSet = set(Y).difference([-1])
	# counting number of instances of each label
	numClassInstances = {}
	for label in labelSet:
		numClassInstances[label] = 0
	for i in range(N):
		if(Y[i] != -1):
			numClassInstances[Y[i]] += 1

	for i in range(e):
		numClassEdge = {}
		for label in labelSet:
			numClassEdge[label] = 0
		for j in range(N):
			if((H[j][i] == 1) and (Y[i] != -1)):
				numClassEdge[Y[i]] += 1
		ratioClassEdge = {}
		for label in labelSet:
			ratioClassEdge[label] = float(numClassEdge[label])/numClassInstances[label]
		
		positiveLabel = max(ratioClassEdge, key=ratioClassEdge.get)
		positiveRatio = ratioClassEdge[positiveLabel]
		
		negativeEdgeInstances = 0
		negativeClassInstances = 0
		for label in labelSet:
			if(label != positiveLabel):
				negativeEdgeInstances += numClassEdge[label]
				negativeClassInstances += numClassInstances[label]
		negativeRatio = float(negativeEdgeInstances)/negativeClassInstances

		# calculating hellinger weight of an edge
		W[i] = (positiveRatio**0.5 - negativeRatio**0.5)**2

	return W
