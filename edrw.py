import numpy as np
import drw
from sklearn.metrics import f1_score

def getTransProb(H, W, N, E):
	# P is the transition probability matrix
	P = [[0 for i in range(N)] for j in range(N)]
	
	# De is the edge degree array
	De = [0 for i in range(E)]
	for i in range(N):
		for k in range(E):
			if(H[i][k] == 1):
				De[k] += 1

	for i in range(N):
		Wsum = 0
		for k in range(E):
			if(H[i][k] == 1):
				Wsum += W[k]

		for j in range(N):
			if(i != j):
				temp = 0
				for k in range(E):
					if((H[i][k] == 1) and (H[j][k] == 1)):
						p1 = 0
						if(Wsum != 0):
							p1 = float(W[k])/Wsum
						p2 = float(1)/(De[k]-1)
						temp += p1*p2
				P[i][j] = temp

	return P

def edrw(H, W, N, E, Y, Ytrue):
	# get the transition probability matrix
	P = getTransProb(H, W, N, E)

	# predict labels for unclassified nodes
	labelDict = drw.classify(N, P, 2, Y)
	
	trueLabels = []
	predictedLabels = []

	total = 0
	score = 0
	for i in range(len(Y)):
		if(Y[i] == -1):
			total += 1
			
			trueLabels.append(Ytrue[i])
			predictedLabels.append(labelDict[i])
			
			if(labelDict[i] == Ytrue[i]):
				# this means correctly classified
				score += 1

	print ("Total Unknowns are:" + str(total))
	print ("Macro f1_score is: " + str(f1_score(trueLabels, predictedLabels, average='macro')))