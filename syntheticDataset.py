import numpy as np
import random

# creates synthetic datasets ---- 2-class dataset

X1_len = 950
X2_len = 50

X1 = [[] for i in range(X1_len)]
X2 = [[] for i in range(X2_len)]

p = 10
Y1 = [1 for i in range(X1_len)]
Y2 = [2 for i in range(X2_len)]

for i in range(p):
	temp1 = random.sample(xrange(1,11), 3)
	temp2 = list(set(xrange(1,11)).difference(temp1))
	
	for j in range(X1_len):
		if(random.random() < 0.75):
			X1[j].append(random.choice(temp1))
		else:
			X1[j].append(random.choice(temp2))
	
	for j in range(X2_len):
		if(random.random() < 0.75):
			X2[j].append(random.choice(temp2))
		else:
			X2[j].append(random.choice(temp1))

X = X1 + X2
Y = Y1 + Y2

np.savetxt("sampleData.csv", X, fmt="%s", delimiter=",")
np.savetxt("sampleLabels.csv", Y, fmt="%s", delimiter=",")