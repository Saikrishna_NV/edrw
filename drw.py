import numpy as np

def getAlphaValue(alpha, q, t, y, Y, P):
	# t value should be >= 1
	if(t < 1):
		print ("error, t:" + str(t))
		return 0

	# return alpha(q, t) value if already calculated otherwise calculate it.
	if(alpha[q][t] != -1):
		return alpha[q][t]

	# alpha(q, t) has to be calculated
	if(t == 1):
		# Ly is the set of all nodes with label y
		Ly = [i for i in range(len(Y)) if Y[i] == y]
		temp = 0
		for u in Ly:
			temp += float(P[u][q])/len(Ly)
		# storing alpha value before returning it
		alpha[q][1] = temp
		return temp
	else:
		# N_Ly is the set of all nodes without label y
		N_Ly = [i for i in range(len(Y)) if Y[i] != y]
		temp = 0
		for u in N_Ly:
			temp += float(P[u][q])*getAlphaValue(alpha, u, t-1, y, Y, P)
		# storing alpha value before returning it
		alpha[q][t] = temp
		return temp

def getBetaValue(beta, q, t, y, Y, P):
	# t value should be >= 1
	if(t < 1):
		print ("error, t:" + str(t))
		return 0

	# return beta(q, t) value if already calculated otherwise calculate it.
	if(beta[q][t] != -1):
		return beta[q][t]

	# beta(q, t) has to be calculated
	if(t == 1):
		# Ly is the set of all nodes with label y
		Ly = [i for i in range(len(Y)) if Y[i] == y]
		temp = 0
		for u in Ly:
			temp += float(P[q][u])
		# storing beta value before returning it
		beta[q][1] = temp
		return temp
	else:
		# N_Ly is the set of all nodes without label y
		N_Ly = [i for i in range(len(Y)) if Y[i] != y]
		temp = 0
		for u in N_Ly:
			temp += float(P[q][u])*getBetaValue(beta, u, t-1, y, Y, P)
		# storing beta value before returning it
		beta[q][t] = temp
		return temp


def getBtwnessValue(q, y, L, alpha, beta, Y, P):
	num = 0
	denom = 0

	# calculating numerator value
	for t in range(1, L+1):
		temp = 0
		for l in range(1, L-t+1):
			temp += getBetaValue(beta, q, l, y, Y, P)
		num += temp*getAlphaValue(alpha, q, t, y, Y, P)

	# calculating denominator value
	for l in range(1, L+1):
		Ly = [i for i in range(len(Y)) if Y[i] == y]
		for u in Ly:
			denom += getAlphaValue(alpha, u, l, y, Y, P)

	# calculating betweenness value
	btw = 0
	if(denom != 0):
		btw = num/denom
	else:
		if(num != 0):
			print "Only denominator is zero"
		else:
			print "Both numerator and denominator are zero"

	return btw

def classify(N, P, L, Y):
	# B is the betweenness array
	B = [{} for i in range(N)]
	# labelSet is the set of all labels, -1 is the label for unclassified node
	labelSet = set(Y).difference([-1])
	unClassifiedNodes = [i for i in range(N) if Y[i] == -1]

	# calculate betweenness values
	for label in labelSet:
		alpha = [[-1 for j in range(L+1)] for i in range(N)]
		beta = [[-1 for j in range(L+1)] for i in range(N)]
		for node in unClassifiedNodes:
			B[node][label] = getBtwnessValue(node, label, L, alpha, beta, Y, P)

	# classify based on the betweenness values
	outLabels = {}
	for node in unClassifiedNodes:
		outLabels[node] = max(B[node], key=B[node].get)

	return outLabels
